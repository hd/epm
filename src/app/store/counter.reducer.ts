import { CounterActions, CounterActionTypes } from './counter.actions';


export const counterFeatureKey = 'counter';

export interface State {
  startedAt: number;
  elapsedSeconds: number;
  totalCount: number;
}

export const initialState: State = {
  startedAt: -1,
  elapsedSeconds : 0,
  totalCount: 0,
};

export function reducer(state = initialState, action: CounterActions): State {
  switch (action.type) {
    case CounterActionTypes.StartCounter: {
      return {
        ...state,
        startedAt: Date.now(),
      };
    }
    case CounterActionTypes.IncreaseCount: {
      return {
        ...state,
        totalCount: state.totalCount + 1,
      };
    }
    case CounterActionTypes.AdvanceSeconds: {
      return {
        ...state,
        elapsedSeconds: state.elapsedSeconds + 1,
      };
    }
    default:
      return state;
  }
}
