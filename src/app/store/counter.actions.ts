import { Action } from '@ngrx/store';

export enum CounterActionTypes {
  StartCounter = '[Counter] Start Counter',
  IncreaseCount = '[Counter] Increase Count',
  AdvanceSeconds = '[Counter] Advance Seconds',

}

export class StartCounter implements Action {
  readonly type = CounterActionTypes.StartCounter;
}

export class IncreaseCount implements Action {
  readonly type = CounterActionTypes.IncreaseCount;
}

export class AdvanceSeconds implements Action {
  readonly type = CounterActionTypes.AdvanceSeconds;
}

export type CounterActions = IncreaseCount
  | StartCounter
  | AdvanceSeconds
  ;
