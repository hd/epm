import {
  ActionReducerMap,
  MetaReducer,
  createSelector
} from '@ngrx/store';
import { environment } from '../../environments/environment';
import * as fromCounter from './counter.reducer';


export interface State {
  [fromCounter.counterFeatureKey]: fromCounter.State;
}

export const reducers: ActionReducerMap<State> = {
  [fromCounter.counterFeatureKey]: fromCounter.reducer,
};


export const metaReducers: MetaReducer<State>[] = !environment.production ? [] : [];

const selectCounter = (state: State) => state[fromCounter.counterFeatureKey];

export const selectStarted = createSelector(
  selectCounter,
  (state: fromCounter.State) => state.startedAt > 0
);

export const totalCount = createSelector(
  selectCounter,
  (state: fromCounter.State) => state.totalCount
);

export const epm = createSelector(
  selectCounter,
  (state: fromCounter.State) => {
    const now = Date.now();
    const secs = Math.floor((now - state.startedAt) / 1000);
    const mins = Math.floor(secs / 60) || 1;
    return state.totalCount / mins;
  }
);

export const seconds = createSelector(
  selectCounter,
  (state: fromCounter.State) => state.elapsedSeconds % 60
);

export const minutes = createSelector(
  selectCounter,
  (state: fromCounter.State) => Math.floor(state.elapsedSeconds / 60)
);


