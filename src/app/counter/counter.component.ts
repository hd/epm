import { Component, OnInit } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { State, selectStarted, totalCount, epm, minutes, seconds } from '../store';
import { CounterService } from './counter.service';
import { IncreaseCount } from '../store/counter.actions';

@Component({
  selector: 'app-counter',
  templateUrl: './counter.component.html',
  styleUrls: ['./counter.component.scss']
})
export class CounterComponent implements OnInit {

  started$ = this.store.pipe(select(selectStarted));
  minutes$ = this.store.pipe(select(minutes));
  seconds$ = this.store.pipe(select(seconds));
  total$ = this.store.pipe(select(totalCount));
  epm$ = this.store.pipe(select(epm));

  constructor(
    private store: Store<State>,
    private counter: CounterService,
  ) { }

  ngOnInit(): void {
  }

  start() {
    this.counter.start();
  }

  erm() {
    this.store.dispatch(new IncreaseCount());
  }
}
