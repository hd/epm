import { TestBed } from '@angular/core/testing';

import { CounterService } from './counter.service';
import { AppStoreModule } from '../store/store.module';

describe('CounterService', () => {
  let service: CounterService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        AppStoreModule,
      ]
    });
    service = TestBed.inject(CounterService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });
});
