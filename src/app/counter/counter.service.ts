import { Injectable } from '@angular/core';
import { timer, Observable, Subscription } from 'rxjs';
import { Store } from '@ngrx/store';
import { State } from '../store';
import { StartCounter, AdvanceSeconds } from '../store/counter.actions';

const INTERVAL = 1000;

@Injectable({
  providedIn: 'root'
})
export class CounterService {
  private ticker: Observable<number>;
  private ticking: Subscription;

  constructor(
    private store: Store<State>,
  ) {
    this.ticker = timer(0, INTERVAL);
  }

  start() {
    if (!this.ticking) {
      this.store.dispatch(new StartCounter());
      this.ticking = this.ticker.subscribe((time) => {
        this.store.dispatch(new AdvanceSeconds());
      });
    }
  }
}
